import xyz.example.Login

def call(String credentialName, String branch, String user, String repo) {
    return new Login(this).commitChanges(credentialName, branch, user, repo)
}
