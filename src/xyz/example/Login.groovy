package xyz.example

class Login implements Serializable {

    def script

    Login(script) {
        this.script = script
    }
    // gitlab-token
    def commitChanges(String credentialName, String branch, String user, String repo) {

        script.withCredentials([script.string(credentialsId: credentialName, variable: "TOKEN")]) {
            script.sh 'git config --global user.email "jenkins@azgcloudev.xyz"'
            script.sh 'git config --global user.name "Jenkins"'

            // url should include username and password
            // Update the Git remote URL to include the GitLab username and personal access token
            script.sh "git remote set-url origin https://${user}:${script.TOKEN}@${repo}"

            script.sh 'git status'
            script.sh 'git add app/package.json'
            script.sh 'git commit -m "jenkins ci: upgrade version code"'
            script.sh "git push origin HEAD:$branch"
        }
    }

}
